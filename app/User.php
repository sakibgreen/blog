<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function blogs()
    {
        return $this->hasMany(Blog::class,'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'user_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class,'user_id');
    }
}
