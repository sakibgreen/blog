<?php

namespace App;
use App\User;
use App\Comment;
use App\Reply;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $guarded=[];

    public function replies()
    {
        return $this->hasMany(Reply::classs);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function users()
    {
    	return $this->belongsTo(User::class);
    }

    public static function user_name($id)
    {
    	$name = User::find($id);
    	if($name != NULL)
    	{
    		return $name->name;
    	}
    	else
    	{
    		return 'Null';
    	}
    }

    


}
