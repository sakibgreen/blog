<?php

namespace App;
use App\Blog;
use App\User;
use App\Reply;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];


    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function blogs()
    {
    	return $this->belongsTo(Blog::class);
    }

    public function users()
    {
    	return $this->belongsTo(User::class);
    }
}
