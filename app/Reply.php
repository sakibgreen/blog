<?php

namespace App;
use App\User;
use App\Comment;
use App\Blog;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $guarded = [];
    
    public function comments()
    {
    	return $this->belongsTo(Comment::class);
    }

    public function blogs()
    {
    	return $this->belongsTo(Blog::class);
    }

    public function users()
    {
    	return $this->belongsTo(User::class);
    }

}
