<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Comment;
use App\Blog;
use App\Reply;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => 'index']);
    }
    
    public function index()
    {
        $all_posts = Blog::latest()->paginate(2);
        return view('blog.index',compact('all_posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->hasFile('image_name'))
        {
            $image_name = $request->image_name->getClientOriginalName();            
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);            

            $blogs = new Blog;
            $blogs->title        = $request->title;
            $blogs->user_id      = auth()->user()->id;
            $blogs->image_name   = $image_name;
            $blogs->image_size   = $image_size;
            $blogs->body         = $request->body;
            $blogs->save(); 

        }  

        return redirect()->route('blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $blogs      = Blog::find($id);
        $blogs_id   = $blogs->id;
        $comments   = Comment::where('post_id',$blogs_id)->get();
        $reply      = Reply::where('blog_id',$blogs_id)->get();
        return view('blog.show',compact('blogs','comments','reply','blogs_id'));
    }

    // $comments   = Comment::orderBy('id', 'DESC')->get();
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function manage()
    {
        if(auth()->check()){
            $blogs = Blog::where('user_id', auth()->user()->id)->get();
            $blogs = Blog::orderBy('id', 'DESC')->get(); //Latest post show first

            return view('blog.manage', compact('blogs'));
        }
        else {
            return view('blog.index');
        }
    }

    /**
    *Manage all post.
    */
    public function edit($id)
    {
        $blogs = Blog::find($id);
        return view ('blog.edit',compact('blogs'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('image_name'))
        {
            $image_name = $request->image_name->getClientOriginalName();            
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);    

            $blogs = Blog::find($id);
            $blogs->update([
                'title'         =>  request('title'),
                'image_name'    =>  $image_name,
                'image_size'    =>  $image_size,
                'body'          =>  request('body')
            ]);
        }
         return redirect()->route('blog.manage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogs = Blog::find($id);
        $blogs->delete();
        return redirect()->route('blog.manage');
    }
}
