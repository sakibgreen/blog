<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Comment;
use App\Blog;
use App\Reply;

class CommentController extends Controller
{
    public function store(Request $request, Blog $blog)
    {
    	$comments 			= new Comment;
    	$comments->comments = $request->comment;
    	// dd($request->all());
    	$comments->user_id  = auth()->user()->id;
    	$comments->post_id 	= $blog->id;
    	$comments->save();

    	return back();
    }

    // public function edit($id)
    // {
    //     $comments = Comment::find($id);
    //     return view('comments.edit',compact('comments'));
    // }

    public function edit($id)
    {
        $comments = Comment::find($id);   
        if(auth()->user()->id === $comments->user_id){
            $comments = Comment::find($id);
            return view('comments.edit',compact('comments'));
        }
        else {
            return view('blog.show');
        }
    }

    public function update(Request $request, $id)
    {
        $comment            = Comment::find($id);
        $post_id            = $comment->post_id;
        $comment->comments  = $request->comment;
        $comment->post_id   = $post_id;
        $comment->save();

        return redirect()->route('blog.show',$post_id); 
    }

    public function destroy($id)
    {
        $comments = Comment::find($id);
        $comments->delete();

        return back();
    }

}
