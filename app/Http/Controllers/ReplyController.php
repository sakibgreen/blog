<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Comment;
use App\Blog;
use App\Reply;
class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Blog $blog,Comment $comment)
    {
        $replies = new Reply;
        $replies->replies       = $request->replies;
        $replies->user_id       = auth()->user()->id;
        $replies->comment_id    = $comment->id;
        $replies->blog_id       = $blog->id;
        // dd($replies);
        $replies->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $reply = Reply::find($id);
        $reply_comment_id   = $reply->comment_id;
        $reply_blog_id      = $reply->blog_id;
        return view ('reply.edit',compact('reply','reply_comment_id','reply_blog_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Blog $blog ,Comment $comment, $id)
    {
        $reply                = Reply::find($id);
        $blog_id              = $reply->blog_id;
        $reply->replies       = $request->reply;
        $reply->user_id       = auth()->user()->id;
        $reply->comment_id    = $comment->id;
        $reply->blog_id       = $blog->id;
        // dd($replies);
        $reply->save();

        return redirect()->route('blog.show',$blog_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $replies = Reply::find($id);
        $replies->delete();

        return back();
    }
}
