@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<form class="form-horizontal mt5" action="/blogs/{{$reply_blog_id}}/comments/{{$reply_comment_id}}/reply/{{$reply->id}}" method="post">
				{{method_field('PUT')}}
        		{{csrf_field()}}
          		<fieldset>
		            <!-- Text input-->
		            <div class="form-group">
		              <label class="col-md-4 control-label" for="">Reply</label>  
		              <div class="col-md-5">
		              <input id="reply" name="reply" type="text" placeholder="write your reply comment" class="form-control input-md" required="" value="{{$reply->replies}}">
		              
		              </div>
		            </div>

		            <!-- Button -->
		            <div class="form-group">
		              <label class="col-md-4 control-label" for="button"></label>
		              <div class="col-md-4">
		                <button id="button" class="btn btn-inverse"><i class="fas fa-save"></i> Save</button>
		              </div> 
		            </div>

          		</fieldset>
     		 </form>
			
		</div>
	</div>

@endsection   