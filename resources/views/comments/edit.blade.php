 @extends('layouts.app')

 @section('content')
    <div class="container">
        <div class="row">
            <form class="form-horizontal mt5" action="{{route('comments.update',['id'=>$comments->id])}}" method="post">
                {{csrf_field()}}
                <div class="col-md-10">
                    <div class="form-group required">
                        <label for="">Comment</label>
                        <textarea name="comment" id="comment" rows="6" class="form-control">{{$comments->comments}}</textarea>
                    </div>
                    <div class="clearfix">
                        <div class="pull-left">
                            <button type="submit" class="btn btn-lg btn-success"><i class="fas fa-save"></i> Submit</button>
                        </div>
                                
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection