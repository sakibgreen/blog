@extends('layouts.app')

@section('content')

	<h1><center>Manage your Post</center></h1>
	<div class="post-meta padding-10 clearfix">
        <div class="pull-left">
            <ul class="post-meta-group">
                <i class="fa fa-user"></i> <blogs>Your Total Blogs: {{$blogs->count()}} </blogs>
            </ul>
        </div>
    </div>        
	<table class="table table-dark">
		<thead>
    		<tr>
    			<th scope="col">Number</th>
			    <th scope="col">Title</th>
			    <th scope="col">image</th>
			    <th scope="col">Body</th>	 
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($blogs as $post)
				<tr>
					<td>{{ $loop->index+1 }}</td>
					<td>{{ $post->title }}</td>  		
	  				<td><img src="{{ asset("storage/upload/".$post->image_name)}}" width="200" height="100"></td>
					<td>{{ $post->body }}</td>
					<td><a href="{{route('blog.edit', $post->id)}}" class="btn btn-warning"><i class="fas fa-edit"></i> Edit</a></td>
					<td><form action="{{route('blog.destroy',$post->id)}}" method="post">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="DELETE">
						<button class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

@endsection