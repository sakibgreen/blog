@extends('layouts.app')

@section('content')

<div class="container">
	<form class="form-horizontal mt-5" action="{{route('blog.store')}}" method="post" enctype="multipart/form-data">
		{{csrf_field()}}
		
		<fieldset>
		
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-12 control-label" for="title">Title</label>  
			  <div class="col-md-12">
			  <input id="title" name="title" type="text" placeholder="Write your title" class="form-control input-md" required="">
			    
			  </div>
			</div>

			<!--Image-->
			<input type="file" name="image_name">

			<!-- Textarea -->
			<div class="form-group">
			  <label class="col-md-12 control-label" for="body">Body</label>
			  <div class="col-md-12">                     
			    <textarea class="form-control" id="body" name="body" required></textarea>
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for=""></label>
			  <div class="col-md-4">
			    <button class="btn btn-warning btn-lg btn-block"><i class="fas fa-save"></i> Save</button>
			  </div>
			</div>

		</fieldset>
	</form>
</div>

@endsection