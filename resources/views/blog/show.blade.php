@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                {{-- <h4>Post ID :{{$blogs->id}}</h4> --}}
                <article class="post-item post-detail">
                    <div class="post-item-image">
                        <img src="{{ asset("storage/upload/" .$blogs->image_name)}}" width="100%" height="100%">
                    </div>

                    <div class="post-item-body">
                        <div class="padding-10">
                            <h3> {{ $blogs->title }} </h3>

                            <div class="post-meta no-border">
                                <ul class="post-meta-group">
                                    <i class="fa fa-user"></i> {{ App\Blog::user_name($blogs->user_id) }} 
                                    <i class="fa fa-clock"></i> <time> {{ $blogs->updated_at->diffForHumans() }}</time>
                                    <i class="fa fa-comment"></i> <comment> {{$comments->count()}} </comment>
                                </ul>
                            </div>

                            <p> {{ $blogs->body }} </p>
                        </div>
                    </div>
                </article>

                <article class="post-comments">
                    <h3><i class="fa fa-comments"></i> Comments</h3>
                    
                    <!--Comment Show-->
                    <div class="comment-body padding-10">
                        @foreach($comments as $comment)
                            <ul class="comments-list">
                                <div class="comment-heading clearfix">
                                    <div class="comment-author-meta">
                                        <h4><i class="fas fa-user"></i> {{ App\Blog::user_name($comment->user_id) }} <small>{{$comment->created_at->diffForHumans()}}</small></h4>
                                    </div>
                                </div>
                                <div class="comment-content">
                                    <p> {{$comment->comments}} </p>
                                    @if ($comment->user_id == auth()->user()->id) 
                                        <form action="{{route('comments.destroy',$comment->id)}}" method="post">
                                            {{csrf_field()}}
                                            <a href="{{route('comments.edit', $comment->id)}}" class="btn btn-default btn-sm"><i class="fas fa-edit"></i> </a>
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i> </button>
                                        </form>
                                    @elseif ($blogs->user_id == auth()->user()->id)
                                        <form action="{{route('comments.destroy',$comment->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i> </button>
                                        </form>
                                    @endif                        
                                </div>

                                <!--Reply Show-->
                                    @foreach( $reply as $r)
                                      @if($r->comment_id == $comment->id)
                                        <ul>
                                            <div class="card-reply-show">
                                                  <div class="card text">
                                                    <h5><i class="fas fa-reply"></i> {{ App\Blog::user_name($r->user_id) }}: {{ $r->replies }} <small>{{$r->updated_at->diffForHumans()}}</small> </h5>
                                                  </div>
                                                @if ($r->user_id == auth()->user()->id)
                                                  <form action="{{route('reply.destroy',$r->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i> </button>
                                                    <a href="{{route('reply.edit', $r->id)}}" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></a>
                                                  </form>
                                                @elseif($comment->user_id == auth()->user()->id)
                                                  <form action="{{route('reply.destroy',$r->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i> </button>
                                                  </form> 
                                                @endif 
                                            </div>  
                                        </ul>
                                      @endif
                                    @endforeach
                                     <!--Reply Comment-->
                                    <form class="form-horizontal mt5" action="/blogs/{{$blogs->id}}/comments/{{$comment->id}}/reply" method="post">
                                        {{csrf_field()}}
                                        <fieldset>
                                            <!-- Text input-->
                                            <div class="titleBox">
                                                <label>Reply Box</label>
                                            </div>
                                            <div class="form-group">
                                                <input id="reply" name="replies" class="form-control" type="text" placeholder="reply comments" />
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-default btn-sm"><i class="fas fa-check-square"></i> Add</button>
                                            </div>
                                        </fieldset>
                                    </form>
                            </ul>
                        @endforeach
                    </div>

                    <!--Input Comment -->
                    <div class="comment-footer padding-10">
                        <h3>Leave a comment</h3>
                        <form class="form-horizontal mt5" action="/blogs/{{$blogs->id}}/comment" method="post">
                            {{csrf_field()}}
                            <div class="form-group required">
                                <label for="comment">Comment</label>
                                <textarea name="comment" id="comment" rows="6" class="form-control" type="text"></textarea>
                            </div>
                            <div class="clearfix">
                                <div class="pull-left">
                                    <button type="submit" class="btn btn-success"><i class="fas fa-check-circle"></i> Submit</button>
                                </div>
                                
                            </div>
                        </form>
                    </div>

                </article>
            </div>
            <div class="col-md-4">
                <aside class="right-sidebar">
                    <div class="search-widget">
                        <div class="input-group">
                          <input type="text" class="form-control input-lg" placeholder="Search for...">
                          <span class="input-group-btn">
                            <button class="btn btn-lg btn-default" type="button">
                                <i class="fa fa-search"></i>
                            </button>
                          </span>
                        </div><!-- /input-group -->
                    </div>

                    <div class="widget">
                        <div class="widget-heading">
                            <h4>Categories</h4>
                        </div>
                        <div class="widget-body">
                            <ul class="categories">
                                <li>
                                    <a href="https://laravel.com/docs/5.5"><i class="fa fa-angle-right"></i> Web Development</a>
                                    <span class="badge pull-right">10</span>
                                </li>
                                <li>
                                    <a href="https://getbootstrap.com/"><i class="fa fa-angle-right"></i> Web Design</a>
                                    <span class="badge pull-right">10</span>
                                </li>
                                <li>
                                    <a href="https://www.w3schools.com/html/"><i class="fa fa-angle-right"></i> W3school</a>
                                    <span class="badge pull-right">10</span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="widget">
                        <div class="widget-heading">
                            <h4>Popular Posts</h4>
                        </div>
                        <div class="widget-body">
                            <ul class="popular-posts">
                                <li>
                                    <div class="post-image">
                                        <a href="#">
                                            <img src="blog_style/img/Post_Image_5_thumb.jpg" />
                                        </a>
                                    </div>
                                    <div class="post-body">
                                        <h6><a href="#">Blog Post #5</a></h6>
                                        <div class="post-meta">
                                            <span>36 minutes ago</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <a href="#">
                                            <img src="blog_style/img/Post_Image_4_thumb.jpg" />
                                        </a>
                                    </div>
                                    <div class="post-body">
                                        <h6><a href="#">Blog Post #4</a></h6>
                                        <div class="post-meta">
                                            <span>36 minutes ago</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <a href="#">
                                            <img src="blog_style/img/Post_Image_3_thumb.jpg" />
                                        </a>
                                    </div>
                                    <div class="post-body">
                                        <h6><a href="#">Blog Post #3</a></h6>
                                        <div class="post-meta">
                                            <span>36 minutes ago</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="widget">
                        <div class="widget-heading">
                            <h4>Tags</h4>
                        </div>
                        <div class="widget-body">
                            <ul class="tags">
                                <li><a href="#">PHP</a></li>
                                <li><a href="#">Codeigniter</a></li>
                                <li><a href="#">Yii</a></li>
                                <li><a href="#">Laravel</a></li>
                                <li><a href="#">Ruby on Rails</a></li>
                                <li><a href="#">jQuery</a></li>
                                <li><a href="#">Vue Js</a></li>
                                <li><a href="#">React Js</a></li>
                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>

@endsection