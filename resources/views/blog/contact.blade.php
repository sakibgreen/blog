@extends('layouts.app')

@section('content')



	<div class="container">
		<!--Image-->
		<div class="card bg-dark text-white">
		  <img class="card-img" src="{{ asset("sakib_image/cover.jpg")}}" width="100%" height="100%" alt="Card image">
		</div><br><br>

		<!--Card-->
		<div class="col-md-12">	
			<div class="col-md-4">
				<div class="card">
			    <img class="card-img-top" src="{{ asset("sakib_image/admin1.png")}}" width="100%" height="100%" alt="Card image cap">
			    <div class="card-body">
			      <h5 class="card-title"><b>Mr.Minhaz Deep</b></h5>
			      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
			      <p class="card-text"><small class="text-muted"><i class="fab fa-internet-explorer"></i> Email:minhaz_deep@gmail.com</small></p>
			    </div>
			  </div>
			</div>
			<div class="col-md-4">
				<div class="card">
			    <img class="card-img-top" src="{{ asset("sakib_image/admin2.png")}}" width="100%" height="100%" alt="Card image cap">
			    <div class="card-body">
			      <h5 class="card-title"><b>Mr.Al Mamun Khan</b></h5>
			      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
			      <p class="card-text"><small class="text-muted"><i class="fab fa-internet-explorer"></i> Email:mamun_khan@gmail.com</small></p>
			    </div>
			  </div>
			</div>
			<div class="col-md-4">
				<div class="card">
			    <img class="card-img-top" src="{{ asset("sakib_image/admin3.png")}}" width="100%" height="100%" alt="Card image cap">
			    <div class="card-body">
			      <h5 class="card-title"><b>Mr.Sakib Al Hasan</b></h5>
			      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
			      <p class="card-text"><small class="text-muted"><i class="fab fa-internet-explorer"></i> Email:sakib_green@gmail.com</small></p>
			    </div>
			  </div>
			</div>
		</div>
	</div>
@endsection