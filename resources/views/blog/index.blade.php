@extends('layouts.app')

@section('content')

    <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <article class="post-item">
                        @foreach($all_posts as $post)
                            {{-- <h5 class="card-post-id">Post ID: {{$post->id}}</h5> --}}
                            <div class="post-item-image">
                                <img src="{{ asset("storage/upload/".$post->image_name)}}" alt="Card image cap">  
                            </div>
                            <div class="post-item-body">
                                <div class="padding-10">
                                    <h2 class="card-title"> {{ $post->title}} </h2>
                                    <p class="card-body">{{ str_limit($post->body, 100) }} </p>
                                </div>

                                <div class="post-meta padding-10 clearfix">
                                    <div class="pull-left">
                                        <ul class="post-meta-group">
                                            <li><i class="fa fa-user"></i> {{ App\Blog::user_name($post->user_id) }}</li>
                                            <li><i class="fa fa-clock"></i> <time>{{$post->updated_at->diffForHumans()}}</time></li>
                                        </ul>                                                                             
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{route('blog.show',$post->id)}}"><i class="fas fa-external-link-alt"></i> Continue Reading</a>
                                    </div>
                                </div>
                            </div><br>
                        @endforeach
                        {{$all_posts->links()}}
                    </article>
                    {{-- <nav>
                      <ul class="pager">
                        <li class="previous"><a href="http://127.0.0.1:8000/blogs/7/show"><span aria-hidden="true">&larr;</span> Newer</a></li>
                        <li class="next"><a href="http://127.0.0.1:8000/blogs/1/show">Older <span aria-hidden="true">&rarr;</span></a></li>
                      </ul>
                    </nav> --}}
                </div>
                <div class="col-md-4">
                    <aside class="right-sidebar">
                        <div class="search-widget">
                            <div class="input-group">
                              <input type="text" class="form-control input-lg" placeholder="Search for...">
                              <span class="input-group-btn">
                                <button class="btn btn-lg btn-default" type="button">
                                    <i class="fas fa-search"></i>
                                </button>
                              </span>
                            </div><!-- /input-group -->
                        </div>

                        <div class="widget">
                            <div class="widget-heading">
                                <h4>Categories</h4>
                            </div>
                            <div class="widget-body">
                                <ul class="categories">
                                    <li>
                                        <a href="https://laravel.com/docs/5.5"><i class="fa fa-angle-right"></i> Web Development</a>
                                        <span class="badge pull-right">10</span>
                                    </li>
                                    <li>
                                        <a href="https://getbootstrap.com/"><i class="fa fa-angle-right"></i> Web Design</a>
                                        <span class="badge pull-right">10</span>
                                    </li>
                                    <li>
                                        <a href="https://www.w3schools.com/"><i class="fa fa-angle-right"></i> W3school</a>
                                        <span class="badge pull-right">10</span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="widget">
                            <div class="widget-heading">
                                <h4>Popular Posts</h4>
                            </div>
                            <div class="widget-body">
                                <ul class="popular-posts">
                                    <li>
                                        <div class="post-image">
                                            <a href="#">
                                                <img src="blog_style/img/Post_Image_5_thumb.jpg" />
                                            </a>
                                        </div>
                                        <div class="post-body">
                                            <h6><a href="http://127.0.0.1:8000/blogs/7/show">Laravel</a></h6>
                                            <div class="post-meta">
                                                <span><time>{{$post->updated_at->diffForHumans()}}</time></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-image">
                                            <a href="">
                                                <img src="blog_style/img/Post_Image_4_thumb.jpg" />
                                            </a>
                                        </div>
                                        <div class="post-body">
                                            <h6><a href="http://127.0.0.1:8000/blogs/6/show">ছাত্রলীগের</a></h6>
                                            <div class="post-meta">
                                                <span><time>{{$post->updated_at->diffForHumans()}}</time></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="post-image">
                                            <a href="#">
                                                <img src="blog_style/img/Post_Image_3_thumb.jpg" />
                                            </a>
                                        </div>
                                        <div class="post-body">
                                            <h6><a href="http://127.0.0.1:8000/blogs/1/show">News paper</a></h6>
                                            <div class="post-meta">
                                                <span><time>{{$post->updated_at->diffForHumans()}}</time></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="widget">
                            <div class="widget-heading">
                                <h4>Tags</h4>
                            </div>
                            <div class="widget-body">
                                <ul class="tags">
                                    <li><a href="http://www.php.net/">PHP</a></li>
                                    <li><a href="https://codeigniter.com/">Codeigniter</a></li>
                                    <li><a href="http://www.yiiframework.com/">Yii</a></li>
                                    <li><a href="https://laravel.com/">Laravel</a></li>
                                    <li><a href="http://rubyonrails.org/">Ruby on Rails</a></li>
                                    <li><a href="#">jQuery</a></li>
                                    <li><a href="#">Vue Js</a></li>
                                    <li><a href="#">React Js</a></li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>    
    </div>
@endsection