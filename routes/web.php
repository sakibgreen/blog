<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Contact
Route::get('/contact', function () {
    return view('blog.contact');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Blog
Route::get('/','BlogController@index')->name('blog.index');
Route::get('create','BlogController@create')->name('blog.create');
Route::post('store','BlogController@store')->name('blog.store');
Route::get('blogs/{id}/show','BlogController@show')->name('blog.show');
Route::get('/manage', 'BlogController@manage')->name('blog.manage');
Route::get('blogs/{blog}/edit', 'BlogController@edit')->name('blog.edit');
Route::post('blogs/{id}', 'BlogController@update')->name('blog.update');
Route::delete('blogs/{blog}', 'BlogController@destroy')->name('blog.destroy');

//Comment
Route::post('/blogs/{blog}/comment','CommentController@store')->name('comments.store');
Route::get('/blogs/{blog}/comment','CommentController@show')->name('comments.show');
Route::get('/comments/{comment}/edit','CommentController@edit')->name('comments.edit');
Route::post('/comments/{id}','CommentController@update')->name('comments.update');
Route::delete('comment/{comment}','CommentController@destroy')->name('comments.destroy');

//Reply
Route::post('/blogs/{blog}/comments/{comment}/reply','ReplyController@store')->name('reply.store');
Route::get('/blogs/{blog}/comments/{comment}/reply','ReplyController@show')->name('replies.show');
Route::delete('reply/{reply}', 'ReplyController@destroy')->name('reply.destroy');
// Route::get('/blogs/{blog}/comments/{comment}/reply/{id}/edit', 'ReplyController@edit')->name('reply.edit');
// Route::put('/blogs/{blog}/comments/{comment}/reply/{id}', 'ReplyController@update')->name('reply.update');

//Another
Route::get('reply/{reply}/edit', 'ReplyController@edit')->name('reply.edit');
Route::put('/blogs/{blog}/comments/{comment}/reply/{id}', 'ReplyController@update')->name('reply.update');

